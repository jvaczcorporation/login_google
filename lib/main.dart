import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(
        title: 'Flutter Demo Home Page',
        googleSignIn: GoogleSignIn(),
        auth: FirebaseAuth.instance,
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final String title;
  final GoogleSignIn googleSignIn;
  final FirebaseAuth auth;

  const MyHomePage({Key key, this.title, this.googleSignIn, this.auth}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            if (widget.auth?.currentUser == null)
              Text(
                "Login ainda não realizado!",
              ),
            if (widget.auth?.currentUser != null)
              Text(
                "Bem Vindo\n${widget.auth?.currentUser?.displayName}",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 35,
                ),
              ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: callLoginGoogle,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  void callLoginGoogle() async {
    final googleUser = await widget.googleSignIn.signIn();

    final googleAuthentication = await googleUser.authentication;

    final authCredential = GoogleAuthProvider.credential(
      idToken: googleAuthentication.idToken,
      accessToken: googleAuthentication.accessToken,
    );

    widget.auth.signInWithCredential(authCredential).whenComplete(() {
      setState(() {});
    });
  }

  Future<void> callLogoutGoogle() async {
    await widget.googleSignIn.signOut();
    await widget.auth.signOut().whenComplete(() {
      setState(() {});
    });
  }
}
